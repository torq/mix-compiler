//
// Created by victor on 16/09/18.
//

#ifndef MIX_INSTRUCTIONS_H
#define MIX_INSTRUCTIONS_H

#include <stdint.h>
#include "../computer.h"

void execute(Instruction *i, Computer *c);

#endif //MIX_INSTRUCTIONS_H
