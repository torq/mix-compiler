//
// Created by victor on 16/09/18.
//

#ifndef MIX_UTILS_H
#define MIX_UTILS_H

#include <stdint.h>
#include "../defs.h"

uint16_t construct(uint8_t first, uint8_t second);

int16_t sw_to_short(SemiWord sw);

int16_t get_mem_address(int16_t address, int8_t index);

SemiWord extract_field_sw(int8_t field, Word *word);

Word extract_field_w(int8_t field, Word *word);

#endif //MIX_UTILS_H
