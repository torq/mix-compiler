//
// Created by victor on 21/09/18.
//

#ifndef MIX_MISCELLANEOUS_H
#define MIX_MISCELLANEOUS_H

#include <stdint.h>
#include "../../computer.h"

void nop(int16_t address, int8_t index, int8_t field, Computer *c);

void special(int16_t address, int8_t index, int8_t field, Computer *c);

void shift(int16_t address, int8_t index, int8_t field, Computer *c);

void move(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_MISCELLANEOUS_H
