//
// Created by victor on 21/09/18.
//

#ifndef MIX_JUMP_OPERATORS_H
#define MIX_JUMP_OPERATORS_H

#include <stdint.h>
#include "../../computer.h"

void jump(int16_t address, int8_t index, int8_t field, Computer *c);

void ja(int16_t address, int8_t index, int8_t field, Computer *c);

void _j1(int16_t address, int8_t index, int8_t field, Computer *c);

void j2(int16_t address, int8_t index, int8_t field, Computer *c);

void j3(int16_t address, int8_t index, int8_t field, Computer *c);

void j4(int16_t address, int8_t index, int8_t field, Computer *c);

void j5(int16_t address, int8_t index, int8_t field, Computer *c);

void j6(int16_t address, int8_t index, int8_t field, Computer *c);

void jx(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_JUMP_OPERATORS_H
