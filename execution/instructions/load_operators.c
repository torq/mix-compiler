//
// Created by victor on 21/09/18.
//
#include "load_operators.h"
#include "../utils.h"

static void ld_sw(int16_t address, int8_t index, int8_t field, Word *mem, SemiWord *reg, unsigned long *timer);
static void ld_w(int16_t address, int8_t index, int8_t field, Word *mem, Word *reg, unsigned long *timer);

void lda(int16_t address, int8_t index, int8_t field, Computer *c) {
    ld_w(address, index, field, c->memory.words, &(c->rA), &(c->timer));
}

void ld1(int16_t address, int8_t index, int8_t field, Computer *c) {
    ld_sw(address, index, field, c->memory.words, &(c->rI[0]), &(c->timer));
}

void ld2(int16_t address, int8_t index, int8_t field, Computer *c) {
    ld_sw(address, index, field, c->memory.words, &(c->rI[1]), &(c->timer));
}

void ld3(int16_t address, int8_t index, int8_t field, Computer *c) {
    ld_sw(address, index, field, c->memory.words, &(c->rI[2]), &(c->timer));
}

void ld4(int16_t address, int8_t index, int8_t field, Computer *c) {
    ld_sw(address, index, field, c->memory.words, &(c->rI[3]), &(c->timer));
}

void ld5(int16_t address, int8_t index, int8_t field, Computer *c) {
    ld_sw(address, index, field, c->memory.words, &(c->rI[4]), &(c->timer));
}

void ld6(int16_t address, int8_t index, int8_t field, Computer *c) {
    ld_sw(address, index, field, c->memory.words, &(c->rI[5]), &(c->timer));
}

void ldx(int16_t address, int8_t index, int8_t field, Computer *c) {
    ld_w(address, index, field, c->memory.words, &(c->rX), &(c->timer));
}

static void ld_sw(int16_t address, int8_t index, int8_t field, Word *mem, SemiWord *reg, unsigned long *timer) {
    int16_t m = get_mem_address(address, index);
    Word data = mem[m];

    *reg = extract_field_sw(field, &data);

    *timer += 2;
}

static void ld_w(int16_t address, int8_t index, int8_t field, Word *mem, Word *reg, unsigned long *timer) {
    int16_t m = get_mem_address(address, index);
    Word data = mem[m];

    *reg = extract_field_w(field, &data);

    *timer += 2;
}