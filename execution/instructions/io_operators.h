//
// Created by victor on 21/09/18.
//

#ifndef MIX_IO_OPERATORS_H
#define MIX_IO_OPERATORS_H

#include <stdint.h>
#include "../../computer.h"

void jbus(int16_t address, int8_t index, int8_t field, Computer *c);

void ioc(int16_t address, int8_t index, int8_t field, Computer *c);

void in(int16_t address, int8_t index, int8_t field, Computer *c);

void out(int16_t address, int8_t index, int8_t field, Computer *c);

void jred(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_IO_OPERATORS_H
