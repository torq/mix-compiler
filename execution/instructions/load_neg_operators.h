//
// Created by victor on 21/09/18.
//

#ifndef MIX_LOAD_NEG_OPERATORS_H
#define MIX_LOAD_NEG_OPERATORS_H

#include <stdint.h>
#include "../../computer.h"

void ldan(int16_t address, int8_t index, int8_t field, Computer *c);

void ld1n(int16_t address, int8_t index, int8_t field, Computer *c);

void ld2n(int16_t address, int8_t index, int8_t field, Computer *c);

void ld3n(int16_t address, int8_t index, int8_t field, Computer *c);

void ld4n(int16_t address, int8_t index, int8_t field, Computer *c);

void ld5n(int16_t address, int8_t index, int8_t field, Computer *c);

void ld6n(int16_t address, int8_t index, int8_t field, Computer *c);

void ldxn(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_LOAD_NEG_OPERATORS_H
