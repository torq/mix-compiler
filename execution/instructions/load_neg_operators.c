//
// Created by victor on 21/09/18.
//

#include "load_neg_operators.h"
#include "../utils.h"

static void ldn_sw(int16_t address, int8_t index, int8_t field, Word *mem, SemiWord *reg, unsigned long *timer);
static void ldn_w(int16_t address, int8_t index, int8_t field, Word *mem, Word *reg, unsigned long *timer);

void ldan(int16_t address, int8_t index, int8_t field, Computer *c) {
    ldn_w(address, index, field, c->memory.words, &(c->rA), &(c->timer));
}

void ld1n(int16_t address, int8_t index, int8_t field, Computer *c) {
    ldn_sw(address, index, field, c->memory.words, &(c->rI[0]), &(c->timer));
}

void ld2n(int16_t address, int8_t index, int8_t field, Computer *c) {
    ldn_sw(address, index, field, c->memory.words, &(c->rI[1]), &(c->timer));
}

void ld3n(int16_t address, int8_t index, int8_t field, Computer *c) {
    ldn_sw(address, index, field, c->memory.words, &(c->rI[2]), &(c->timer));
}

void ld4n(int16_t address, int8_t index, int8_t field, Computer *c) {
    ldn_sw(address, index, field, c->memory.words, &(c->rI[3]), &(c->timer));
}

void ld5n(int16_t address, int8_t index, int8_t field, Computer *c) {
    ldn_sw(address, index, field, c->memory.words, &(c->rI[4]), &(c->timer));
}

void ld6n(int16_t address, int8_t index, int8_t field, Computer *c) {
    ldn_sw(address, index, field, c->memory.words, &(c->rI[5]), &(c->timer));
}

void ldxn(int16_t address, int8_t index, int8_t field, Computer *c) {
    ldn_w(address, index, field, c->memory.words, &(c->rX), &(c->timer));
}

static void ldn_sw(int16_t address, int8_t index, int8_t field, Word *mem, SemiWord *reg, unsigned long *timer) {
    int16_t m = get_mem_address(address, index);
    Word data = mem[m];

    SemiWord temp = extract_field_sw(field, &data);
    temp.sign = (temp.sign) ? (uint8_t)0 : (uint8_t)1;
    *reg = temp;

    *timer += 2;
}

static void ldn_w(int16_t address, int8_t index, int8_t field, Word *mem, Word *reg, unsigned long *timer) {
    int16_t m = get_mem_address(address, index);
    Word data = mem[m];

    Word temp = extract_field_w(field, &data);
    temp.sign = (temp.sign) ? (uint8_t)0 : (uint8_t)1;
    *reg = temp;

    *timer += 2;
}
