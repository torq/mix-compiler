//
// Created by victor on 21/09/18.
//

#include "miscellaneous_operators.h"
#include "../utils.h"

void nop(int16_t address, int8_t index, int8_t field, Computer *c) {
    c->timer += 1;
}

void special(int16_t address, int8_t index, int8_t field, Computer *c) {
    // TODO: implement special
    c->timer += 10;
}

void shift(int16_t address, int8_t index, int8_t field, Computer *c) {
    // TODO: implement shift
    c->timer += 2;
}

void move(int16_t address, int8_t index, int8_t field, Computer *c) {
    // TODO: implement move
    c->timer += (1 + 2*field);
}
