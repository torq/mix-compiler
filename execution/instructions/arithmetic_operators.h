//
// Created by victor on 21/09/18.
//

#ifndef MIX_ARITHMETIC_OPERATORS_H
#define MIX_ARITHMETIC_OPERATORS_H

#include <stdint.h>
#include "../../computer.h"

void add(int16_t address, int8_t index, int8_t field, Computer *c);

void sub(int16_t address, int8_t index, int8_t field, Computer *c);

void mul(int16_t address, int8_t index, int8_t field, Computer *c);

void _div(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_ARITHMETIC_OPERATORS_H
