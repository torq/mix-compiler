//
// Created by victor on 21/09/18.
//

#ifndef MIX_COMPARISON_OPERATORS_H
#define MIX_COMPARISON_OPERATORS_H

#include <stdint.h>
#include "../../computer.h"

void cmpa(int16_t address, int8_t index, int8_t field, Computer *c);

void cmp1(int16_t address, int8_t index, int8_t field, Computer *c);

void cmp2(int16_t address, int8_t index, int8_t field, Computer *c);

void cmp3(int16_t address, int8_t index, int8_t field, Computer *c);

void cmp4(int16_t address, int8_t index, int8_t field, Computer *c);

void cmp5(int16_t address, int8_t index, int8_t field, Computer *c);

void cmp6(int16_t address, int8_t index, int8_t field, Computer *c);

void cmpx(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_COMPARISON_OPERATORS_H
