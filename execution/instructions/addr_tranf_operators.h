//
// Created by victor on 21/09/18.
//

#ifndef MIX_ADDR_TRANF_OPERATORS_H
#define MIX_ADDR_TRANF_OPERATORS_H

#include <stdint.h>
#include "../../computer.h"

void addr_tr_a(int16_t address, int8_t index, int8_t field, Computer *c);

void addr_tr_1(int16_t address, int8_t index, int8_t field, Computer *c);

void addr_tr_2(int16_t address, int8_t index, int8_t field, Computer *c);

void addr_tr_3(int16_t address, int8_t index, int8_t field, Computer *c);

void addr_tr_4(int16_t address, int8_t index, int8_t field, Computer *c);

void addr_tr_5(int16_t address, int8_t index, int8_t field, Computer *c);

void addr_tr_6(int16_t address, int8_t index, int8_t field, Computer *c);

void addr_tr_x(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_ADDR_TRANF_OPERATORS_H
