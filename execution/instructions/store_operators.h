//
// Created by victor on 21/09/18.
//

#ifndef MIX_STORE_OPERATORS_H
#define MIX_STORE_OPERATORS_H

#include <stdint.h>
#include "../../computer.h"

void sta(int16_t address, int8_t index, int8_t field, Computer *c);

void st1(int16_t address, int8_t index, int8_t field, Computer *c);

void st2(int16_t address, int8_t index, int8_t field, Computer *c);

void st3(int16_t address, int8_t index, int8_t field, Computer *c);

void st4(int16_t address, int8_t index, int8_t field, Computer *c);

void st5(int16_t address, int8_t index, int8_t field, Computer *c);

void st6(int16_t address, int8_t index, int8_t field, Computer *c);

void stx(int16_t address, int8_t index, int8_t field, Computer *c);

void stj(int16_t address, int8_t index, int8_t field, Computer *c);

void stz(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_STORE_OPERATORS_H
