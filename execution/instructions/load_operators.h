//
// Created by victor on 21/09/18.
//

#ifndef MIX_LOAD_OPERATORS_H
#define MIX_LOAD_OPERATORS_H

#include <stdint.h>
#include "../../computer.h"

void lda(int16_t address, int8_t index, int8_t field, Computer *c);

void ld1(int16_t address, int8_t index, int8_t field, Computer *c);

void ld2(int16_t address, int8_t index, int8_t field, Computer *c);

void ld3(int16_t address, int8_t index, int8_t field, Computer *c);

void ld4(int16_t address, int8_t index, int8_t field, Computer *c);

void ld5(int16_t address, int8_t index, int8_t field, Computer *c);

void ld6(int16_t address, int8_t index, int8_t field, Computer *c);

void ldx(int16_t address, int8_t index, int8_t field, Computer *c);

#endif //MIX_LOAD_OPERATORS_H
