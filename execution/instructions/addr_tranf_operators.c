//
// Created by victor on 21/09/18.
//

#include "addr_tranf_operators.h"

void inca(int16_t address, Computer *c) {
    // TODO: implement inca
}

void deca(int16_t address, Computer *c) {
    // TODO: implement deca
}

void enta(int16_t address, Computer *c) {
    // TODO: implement enta
}

void enna(int16_t address, Computer *c) {
    // TODO: implement enna
}

void addr_tr_a(int16_t address, int8_t index, int8_t field, Computer *c) {
    int16_t full_address = 0;
    switch (field) {
        case 0:
            inca(full_address, c);
            break;
        case 1:
            deca(address, c);
            break;
        case 2:
            enta(address, c);
            break;
        case 3:
            enna(address, c);
            break;
        default:
            break;

    }
    c->timer += 1;
}

void inc1(int16_t address, Computer *c) {
    // TODO: implement inc1
}

void dec1(int16_t address, Computer *c) {
    // TODO: implement dec1
}

void ent1(int16_t address, Computer *c) {
    // TODO: implement ent1
}

void enn1(int16_t address, Computer *c) {
    // TODO: implement enn1
}

void addr_tr_1(int16_t address, int8_t index, int8_t field, Computer *c) {
    int16_t full_address = 0;
    switch (field) {
        case 0:
            inc1(full_address, c);
            break;
        case 1:
            dec1(address, c);
            break;
        case 2:
            ent1(address, c);
            break;
        case 3:
            enn1(address, c);
            break;
        default:
            break;

    }
    c->timer += 1;
}

void inc2(int16_t address, Computer *c) {
    // TODO: implement inc2
}

void dec2(int16_t address, Computer *c) {
    // TODO: implement dec2
}

void ent2(int16_t address, Computer *c) {
    // TODO: implement ent2
}

void enn2(int16_t address, Computer *c) {
    // TODO: implement enn2
}

void addr_tr_2(int16_t address, int8_t index, int8_t field, Computer *c) {
    int16_t full_address = 0;
    switch (field) {
        case 0:
            inc2(full_address, c);
            break;
        case 1:
            dec2(address, c);
            break;
        case 2:
            ent2(address, c);
            break;
        case 3:
            enn2(address, c);
            break;
        default:
            break;

    }
    c->timer += 1;
}

void inc3(int16_t address, Computer *c) {
    // TODO: implement inc3
}

void dec3(int16_t address, Computer *c) {
    // TODO: implement dec3
}

void ent3(int16_t address, Computer *c) {
    // TODO: implement ent3
}

void enn3(int16_t address, Computer *c) {
    // TODO: implement enn3
}

void addr_tr_3(int16_t address, int8_t index, int8_t field, Computer *c) {
    int16_t full_address = 0;
    switch (field) {
        case 0:
            inc3(full_address, c);
            break;
        case 1:
            dec3(address, c);
            break;
        case 2:
            ent3(address, c);
            break;
        case 3:
            enn3(address, c);
            break;
        default:
            break;

    }
    c->timer += 1;
}

void inc4(int16_t address, Computer *c) {
    // TODO: implement inc4
}

void dec4(int16_t address, Computer *c) {
    // TODO: implement dec4
}

void ent4(int16_t address, Computer *c) {
    // TODO: implement ent4
}

void enn4(int16_t address, Computer *c) {
    // TODO: implement enn4
}

void addr_tr_4(int16_t address, int8_t index, int8_t field, Computer *c) {
    int16_t full_address = 0;
    switch (field) {
        case 0:
            inc4(full_address, c);
            break;
        case 1:
            dec4(address, c);
            break;
        case 2:
            ent4(address, c);
            break;
        case 3:
            enn4(address, c);
            break;
        default:
            break;

    }
    c->timer += 1;
}

void inc5(int16_t address, Computer *c) {
    // TODO: implement inc5
}

void dec5(int16_t address, Computer *c) {
    // TODO: implement dec5
}

void ent5(int16_t address, Computer *c) {
    // TODO: implement ent5
}

void enn5(int16_t address, Computer *c) {
    // TODO: implement enn5
}

void addr_tr_5(int16_t address, int8_t index, int8_t field, Computer *c) {
    int16_t full_address = 0;
    switch (field) {
        case 0:
            inc5(full_address, c);
            break;
        case 1:
            dec5(address, c);
            break;
        case 2:
            ent5(address, c);
            break;
        case 3:
            enn5(address, c);
            break;
        default:
            break;

    }
    c->timer += 1;
}

void inc6(int16_t address, Computer *c) {
    // TODO: implement inc6
}

void dec6(int16_t address, Computer *c) {
    // TODO: implement dec6
}

void ent6(int16_t address, Computer *c) {
    // TODO: implement ent6
}

void enn6(int16_t address, Computer *c) {
    // TODO: implement enn6
}

void addr_tr_6(int16_t address, int8_t index, int8_t field, Computer *c) {
    int16_t full_address = 0;
    switch (field) {
        case 0:
            inc6(full_address, c);
            break;
        case 1:
            dec6(address, c);
            break;
        case 2:
            ent6(address, c);
            break;
        case 3:
            enn6(address, c);
            break;
        default:
            break;

    }
    c->timer += 1;
}

void incx(int16_t address, Computer *c) {
    // TODO: implement incx
}

void decx(int16_t address, Computer *c) {
    // TODO: implement decx
}

void entx(int16_t address, Computer *c) {
    // TODO: implement entx
}

void ennx(int16_t address, Computer *c) {
    // TODO: implement ennx
}

void addr_tr_x(int16_t address, int8_t index, int8_t field, Computer *c) {
    int16_t full_address = 0;
    switch (field) {
        case 0:
            incx(full_address, c);
            break;
        case 1:
            decx(address, c);
            break;
        case 2:
            entx(address, c);
            break;
        case 3:
            ennx(address, c);
            break;
        default:
            break;

    }
    c->timer += 1;
}
