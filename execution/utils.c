//
// Created by victor on 16/09/18.
//

#include <stdlib.h>
#include "utils.h"

uint16_t construct(uint8_t first, uint8_t second) {
    first &= 0x3f;
    second &= 0x3f;

    uint16_t result = first;
    result <<= BYTE_S;
    result |= second;

    return result;
}

int16_t sw_to_short(SemiWord sw) {
    uint16_t value = construct(sw.byte1, sw.byte2);
    int8_t sign = (sw.sign) ? (uint8_t)1 : (uint8_t)-1;
    return value*sign;
}

int16_t get_mem_address(int16_t address, int8_t index) {
    int16_t m = address + index;
    return m;
}

SemiWord extract_field_sw(int8_t field, Word *word) {
    int l = field/8;
    int r = field%8;

    if(l>r)
        exit(ERR_AI);

    SemiWord new_sw;
    switch (l) {
        case 0:
            new_sw.sign = word->sign;
            if(r == 0) break;
        case 1:
            if(r == 1) break;
        case 2:
            if(r == 2) break;
        case 3:
            if(r == 3) break;
        case 4:
            new_sw.byte1 = word->byte4;
            if(r == 4) break;
        case 5:
            new_sw.byte2 = word->byte5;
            break;
        default:
            exit(ERR_OOB);
    }

    return new_sw;
}

Word extract_field_w(int8_t field, Word *word) {
    int l = field/8;
    int r = field%8;

    if(l>r)
        exit(ERR_AI);

    Word new_word;
    switch (l) {
        case 0:
            new_word.sign = word->sign;
            if(r == 0) break;
        case 1:
            new_word.byte1 = word->byte1;
            if(r == 1) break;
        case 2:
            new_word.byte2 = word->byte2;
            if(r == 2) break;
        case 3:
            new_word.byte3 = word->byte3;
            if(r == 3) break;
        case 4:
            new_word.byte4 = word->byte4;
            if(r == 4) break;
        case 5:
            new_word.byte5 = word->byte5;
            break;
        default:
            exit(ERR_OOB);
    }

    return new_word;
}