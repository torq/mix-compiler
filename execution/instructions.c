//
// Created by victor on 16/09/18.
//

#include <stdlib.h>
#include "instructions.h"
#include "utils.h"
#include "instructions/miscellaneous_operators.h"
#include "instructions/arithmetic_operators.h"
#include "instructions/load_operators.h"
#include "instructions/load_neg_operators.h"
#include "instructions/store_operators.h"
#include "instructions/io_operators.h"
#include "instructions/jump_operators.h"
#include "instructions/addr_tranf_operators.h"
#include "instructions/comparison_operators.h"

static int16_t get_address(Instruction *i);
static void execute_instruction(int16_t address, int8_t index, int8_t field, int8_t code, Computer *c);

void execute(Instruction *i, Computer *c) {
    int16_t address = get_address(i);
    uint8_t index = i->index;
    uint8_t field = i->field;
    uint8_t code = i->code;

    execute_instruction(address, index, field, code, c);
}

static int16_t get_address(Instruction *i) {
    return sw_to_short((SemiWord) {i->sign, i->address1, i->address2});
}

static void execute_instruction(int16_t address, int8_t index, int8_t field, int8_t code, Computer *c) {
    static void (*instr[64]) (int16_t address, int8_t index, int8_t field, Computer *c) = {
        nop, // MISC OPERATORS
        add, sub, mul, _div, // ARITHMETIC OPERATORS
        special, shift, move, // MISC OPERATORS
        lda, ld1, ld2, ld3, ld4, ld5, ld6, ldx, // LOAD OPERATORS
        ldan, ld1n, ld2n, ld3n, ld4n, ld5n, ld6n, ldxn, // LOAD NEGATIVE OPERATORS
        sta, st1, st2, st3, st4, st5, st6, stx, stj, stz, // STORE OPERATORS
        jbus, ioc, in, out, jred, // IO OPERATORS
        jump, ja, _j1, j2, j3, j4, j5, j6, jx, // JUMP OPERATORS
        addr_tr_a, addr_tr_1, addr_tr_2, addr_tr_3, addr_tr_4, addr_tr_5, addr_tr_6, addr_tr_x, // ADDRESS TRANSFER OPERATORS
        cmpa, cmp1, cmp2, cmp3, cmp4, cmp5, cmp6, cmpx // COMPARISON OPERATORS
    };

    if(code > 63)
        exit(ERR_OOB);

    (*instr[code]) (address, index, field, c);
}