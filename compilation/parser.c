//
// Created by victor on 23/09/18.
//

#include <stdlib.h>
#include "parser.h"
#include "../utils/file_util.h"
#include "../utils/string_util.h"

static Instruction parse_line(char* line);
static char** tokenize(const char* line);

Instruction* parse(FILE *file) {
    char **lines;
    int n_lines = file_to_strings(file, lines);
    Instruction* instructions = (Instruction *)malloc(n_lines * sizeof(Instruction));

    int i = 0;
    for(char* line = *lines; line;) {
        instructions[i++] = parse_line(line++);
        free(lines[i]);
    }

    free(lines);
    return instructions;
}

static Instruction parse_line(char* line) {
    Instruction instruction = {0};

    char** tokens = tokenize(line);

    // TODO: get what is what

    return instruction;
}

static char** tokenize(const char* line) {
    // replace ',' by ' '
    char* no_commas = replace(line, ',', ' ');

    // replace '(' by ' '
    char* no_open_parenthesis = replace(no_commas, '(', ' ');
    free(no_commas);

    // replace ')' by ' '
    char* no_close_parenthesis = replace(no_open_parenthesis, ')', ' ');
    free(no_open_parenthesis);

    // replace ':' by ' '
    char* no_colons = replace(no_close_parenthesis, ':', ' ');
    free(no_close_parenthesis);

    // split by ' '
    char** tokens = split(no_colons, ' ');
    free(no_colons);

    return tokens;
}
