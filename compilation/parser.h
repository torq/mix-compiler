//
// Created by victor on 23/09/18.
//

#ifndef MIX_PARSER_H
#define MIX_PARSER_H

#include <stdio.h>
#include "../defs.h"

Instruction* parse(FILE *file);

#endif //MIX_PARSER_H
