#include "compiler.h"

#include <stdio.h>
#include <stdlib.h>

#include "parser.h"
#include "../defs.h"
#include "../utils/file_util.h"

void compile(char *input_file, char *output_file) {
    // read input file, parse it and compile it
    FILE *input;
    open_file_hard(input_file, "r", input);
    Instruction *instructions = parse(input);
    fclose(input);

    // write the resulting instructions in binary format
    FILE *output;
    open_file_hard(output_file, "wb", output);
    for (Instruction *temp = instructions; temp; temp++)
        fwrite(temp, sizeof(Instruction), 1, output);

    fclose(output);
}