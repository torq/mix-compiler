//
// Created by victor on 28/09/18.
//

#ifndef MIX_STRING_UTIL_H
#define MIX_STRING_UTIL_H

int count_char(const char* const input, const char token);
char* replace(const char* const input, const char replaceable, const char replacer);
char** split(const char* const input, const char delimiter);

#endif //MIX_STRING_UTIL_H
