//
// Created by victor on 28/09/18.
//

#include "string_util.h"

#include <stdlib.h>
#include <string.h>

char* replace(const char* const input, const char replaceable, const char replacer) {
    int len = strlen(input);
    char* new = (char *)malloc(len+1 * sizeof(char));

    for(int i=0; i<=len; ++i) {
        new[i] = (input[i] == replaceable)
                 ? replacer
                 : input[i];
    }

    return new;
}

int count_char(const char* const input, const char token) {
    int c = 0;
    for(const char* temp = input; *temp; ++temp)
        if(*temp == token)
            ++c;
    return c;
}

char** split(const char* const input, const char delimiter) {
    int buffer_size = 2;

    char *buffer =(char *)malloc(buffer_size * sizeof(char));

    int n_tokens = count_char(input, delimiter);
    char **lines = (char **)malloc((n_tokens+2) * sizeof(char *));
    for(int i=0, j=0, k=0;; ++i) {
        if(k >= buffer_size) {
            buffer_size <<= 1;
            buffer = (char *)realloc(buffer, buffer_size * sizeof(char));
        }
        char token = input[i];
        if(token == delimiter) {
            buffer[k] = '\0';
            if(*buffer) {
                buffer = (char *)realloc(buffer, (k+1) * sizeof(char));
                lines[j++] = buffer;
            }
            buffer = (char *)malloc((buffer_size = 2) * sizeof(char));
            k = 0;
        } else if(token == '\0') {
            buffer[k] = '\0';
            if(*buffer) {
                buffer = (char *)realloc(buffer, (k+1) * sizeof(char));
                lines[j++] = buffer;
            }
            buffer = NULL;
            lines[j] = NULL;
            break;
        } else {
            buffer[k++] = token;
        }
    }
    return lines;
}
