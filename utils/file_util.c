//
// Created by victor on 24/09/18.
//

#include <stdlib.h>
#include "file_util.h"
#include "../defs.h"
#include "string_util.h"

void open_file_hard(const char *path, const char *mode, FILE *file) {
    file = fopen(path, mode);
    if (!file) {
        fprintf(stderr, "Unable to open %s\n", path);
        exit(ERR_INIT);
    }
}

char* file_to_string(FILE *file) {
    // get file size
    fseek(file, 0, SEEK_END);
    long fsize = ftell(file);
    fseek(file, 0, SEEK_SET);

    // store the entire file inside a string
    char *string = malloc(fsize+1);
    fread(string, fsize, 1, file);
    string[fsize] = 0;

    return string;
}

int file_to_strings(FILE *file, char **lines) {
    char *string = file_to_string(file);
    lines = split(string, '\n');

    int c = 0;
    for(int i=0; lines[i]; ++i)
        ++c;

    return c;
}