//
// Created by victor on 16/09/18.
//

#ifndef MIX_COMPUTER_H
#define MIX_COMPUTER_H

#include <stdint.h>
#include <stdbool.h>
#include "defs.h"

typedef Word RegisterA;
typedef Word RegisterX;
typedef SemiWord RegisterI;
typedef SemiWord RegisterJ;

typedef struct c {
    uint8_t less:    1;
    uint8_t equal:   1;
    uint8_t greater: 1;
} ComparisonIndicator;

typedef struct u {
    uint8_t byte1: BYTE_S;
} IOUnit;

typedef struct m {
    Word words[4000];
} Memory;

typedef struct computer {
    RegisterA rA; // 5
    RegisterX rX; // 5

    RegisterI rI[6]; // 3*6 = 18
    RegisterJ rJ; // 3

    bool overflow_toggle; // 1
    ComparisonIndicator comparisonIndicator; // 1

    IOUnit u[21]; //21

    Memory memory;

    unsigned long timer;
} Computer;

#endif //MIX_COMPUTER_H
