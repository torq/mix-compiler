#include <stdio.h>
#include <stdlib.h>
#include "computer.h"
#include "compilation/compiler.h"
#include "execution/instructions.h"
#include "utils/file_util.h"

#define INPUT_FILE argv[1]
#define EXE_FILE "./a.out"

 int main(int argc, char** argv) {
    printf(":: Compiling %s ...\n", INPUT_FILE);
    compile(INPUT_FILE, EXE_FILE);
    printf(":: %s compiled to %s\n", INPUT_FILE, EXE_FILE);

    return 0;
}