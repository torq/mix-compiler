//
// Created by victor on 16/09/18.
//

#ifndef MIX_DEFS_H
#define MIX_DEFS_H

#include <stdint.h>

// ERRORS
#define ERR_OOB 1
#define ERR_AI  2
#define ERR_INIT 3

#define BYTE_S 6

typedef  struct sw {
    uint8_t sign:  BYTE_S;
    uint8_t byte1: BYTE_S;
    uint8_t byte2: BYTE_S;
} SemiWord;

typedef struct w {
    uint8_t sign:  BYTE_S;
    uint8_t byte1: BYTE_S;
    uint8_t byte2: BYTE_S;
    uint8_t byte3: BYTE_S;
    uint8_t byte4: BYTE_S;
    uint8_t byte5: BYTE_S;
} Word;

typedef struct i {
    uint8_t sign: BYTE_S;
    uint8_t address1: BYTE_S;
    uint8_t address2: BYTE_S;
    uint8_t index: BYTE_S;
    uint8_t field: BYTE_S;
    uint8_t code: BYTE_S;
} Instruction;

#endif //MIX_DEFS_H
